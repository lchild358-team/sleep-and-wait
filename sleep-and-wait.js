module.exports = (function(){

  const sleep = require('system-sleep');

  const Func = function(ms){

    const data = { time: ms };

    const _exec = function(){

      const starttime = new Date();

      let waitflg = true;

      if(data.promise){

        data.promise.then(()=>{
          waitflg = false;
        }, ()=>{
          waitflg = false;
        });

      }else if(data.condition){

        waitflg = ! data.condition();

      }else if(data.stream){

        data.stream.on('end', ()=>{
          waitflg = false;
        });
        data.stream.on('close', ()=>{
          waitflg = false;
        });

      }

      while(waitflg){
        sleep(data.time);

        if(data.timeout && new Date() - starttime >= data.timeout){
          throw new Error('Waiting timed out');
        }

        if(data.condition){
          waitflg = ! data.condition();
        }
      }
    };

    const _timeout = function(ms){
      data.timeout = ms;
      _exec();
    };

    const _notimeout = function(){
      data.timeout = false;
      _exec();
    };

    const _ret_timeout = {
      timeout: _timeout,
      no     : {
        timeout: _notimeout
      }
    };

    const _promise = function(promise){
      data.promise = promise;
      return _ret_timeout;
    };

    const _condition = function(cond){
      data.condition = cond;
      return _ret_timeout;
    };

    const _stream = function(stream){
      data.stream = stream;
      return _ret_timeout;
    };

    return {
      for: {
        promise  : _promise,
        condition: _condition,
        stream   : _stream
      }
    };
  };

  return Func;

})();
