# sleep-and-wait

Sleep and wait for asynchronous processing to complete.

## Install
```bash
npm install sleep-and-wait
```

## Usage
```javascript
const sleep = require('sleep-and-wait');

sleep(100).for.promise(...).timeout(10000);
// Sleep every 100ms waiting for the promise to complete, timeout after 10s

sleep(50).for.condition(()=>{...}).timeout(5000);
// Sleep every 50ms waiting for the condition become true, timeout after 5s

sleep(10).for.stream(...).timeout(2000);
// Sleep every 10ms waiting for the stream to end or close, timeout after 2s
```

## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

## License
[ISC](https://opensource.org/licenses/ISC)

